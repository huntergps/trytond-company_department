# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.transaction import Transaction

__all__ = ['Department', 'DepartmentUser']


class Department(ModelSQL, ModelView):
    'Department'
    __name__ = 'company.department'
    name = fields.Char('Name', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    users = fields.Many2Many('res.user-company.department',
        'department', 'user', 'Users')

    @classmethod    
    def __setup__(cls):
        super(Department, cls).__setup__()
        cls._order.insert(0, ('name', 'ASC'))

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False


class DepartmentUser(ModelSQL):
    'Department User'
    __name__ = 'res.user-company.department'
    _table = 'res_user_company_department'
    department = fields.Many2One('company.department', 'Department',
        ondelete='CASCADE', select=True,  required=True)
    user = fields.Many2One('res.user', 'User', select=True, 
        required=True, ondelete='RESTRICT')